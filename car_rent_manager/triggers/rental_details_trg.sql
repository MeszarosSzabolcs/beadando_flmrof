create or replace trigger rental_details_trg
before insert or update on rental_details
for each row 
begin 
    if(inserting) then
       if(:new.rent_id is null) then
          :new.rent_id := rent_id_seq.nextval;
       end if;
       :new.created_date := sysdate;
       :new.last_modified := sysdate;
       :new.dml_flag := 'I';
       :new.version := 1;
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
    else
       :new.last_modified := sysdate;
       :new.dml_flag := 'U';
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
       :new.version := :old.version + 1;
    end if;
end;
/

create or replace trigger rental_details_h_trg
after delete or update or insert on rental_details
for each row
begin
    if (deleting) then
       insert into rental_details_h(rent_id,
                              client_id,
                              car_id,
                              from_date,
                              ret_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.rent_id, 
                             :old.client_id, 
                             :old.car_id, 
                             :old.from_date, 
                             :old.ret_date, 
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into rental_details_h(rent_id,
                              client_id,
                              car_id,
                              from_date,
                              ret_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.rent_id,
                              :new.client_id,
                              :new.car_id,
                              :new.from_date,
                              :new.ret_date,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;