CREATE TRIGGER rent_id_seq_trigger
BEFORE INSERT ON rental_details
FOR EACH ROW
BEGIN
    IF(:new.rent_id is null) then
    :new.rent_id := RENT_ID_SEQ.nextval;
    END IF;
END;