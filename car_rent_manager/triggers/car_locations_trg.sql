create or replace trigger car_locations_trg
before insert or update on car_locations
for each row 
begin 
    if(inserting) then
       if(:new.location_id is null) then
          :new.location_id := location_id_seq.nextval;
       end if;
       :new.created_date := sysdate;
       :new.last_modified := sysdate;
       :new.dml_flag := 'I';
       :new.version := 1;
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
    else
       :new.last_modified := sysdate;
       :new.dml_flag := 'U';
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
       :new.version := :old.version + 1;
    end if;
end;
/

create or replace trigger car_locations_h_trg
after delete or update or insert on car_locations
for each row
begin
    if (deleting) then
       insert into car_locations_h(location_id,
                              city,
                              street_name,
                              building_number,
                              zip_code,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.location_id, 
                             :old.city, 
                             :old.street_name, 
                             :old.building_number, 
                             :old.zip_code,
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into car_locations_h(location_id,
                              city,
                              street_name,
                              building_number,
                              zip_code,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.location_id,
                              :new.city,
                              :new.street_name,
                              :new.building_number,
                              :new.zip_code,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;