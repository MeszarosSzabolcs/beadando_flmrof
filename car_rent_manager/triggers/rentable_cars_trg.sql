create or replace trigger rentable_cars_trg
before insert or update on rentable_cars
for each row 
begin 
    if(inserting) then
       if(:new.car_id is null) then
          :new.car_id := car_id_seq.nextval;
       end if;
       :new.created_date := sysdate;
       :new.last_modified := sysdate;
       :new.dml_flag := 'I';
       :new.version := 1;
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
    else
       :new.last_modified := sysdate;
       :new.dml_flag := 'U';
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
       :new.version := :old.version + 1;
    end if;
end;
/

create or replace trigger rentable_cars_h_trg
after delete or update or insert on rentable_cars
for each row
begin
    if (deleting) then
       insert into rentable_cars_h(car_id,
                              category_id,
                              model_name,
                              make,
                              fuel,
                              gears,
                              mileage,
                              seats,
                              year,
                              location_id,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.car_id, 
                             :old.category_id, 
                             :old.model_name, 
                             :old.make, 
                             :old.fuel, 
                             :old.gears,
                             :old.mileage,
                             :old.seats,
                             :old.year,
                             :old.location_id,
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into rentable_cars_h(car_id,
                              category_id,
                              model_name,
                              make,
                              fuel,
                              gears,
                              mileage,
                              seats,
                              year,
                              location_id,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.car_id,
                              :new.category_id,
                              :new.model_name,
                              :new.make,
                              :new.fuel,
                              :new.gears,
                              :new.mileage,
                              :new.seats,
                              :new.year,
                              :new.location_id,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;