create or replace trigger car_categories_trg
before insert or update on car_categories
for each row 
begin 
    if(inserting) then
       if(:new.category_id is null) then
          :new.category_id := category_id_seq.nextval;
       end if;
       :new.created_date := sysdate;
       :new.last_modified := sysdate;
       :new.dml_flag := 'I';
       :new.version := 1;
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
    else
       :new.last_modified := sysdate;
       :new.dml_flag := 'U';
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
       :new.version := :old.version + 1;
    end if;
end;
/

create or replace trigger car_categories_h_trg
after delete or update or insert on car_categories
for each row
begin
    if deleting then
       insert into car_categories_h(category_id,
                          category_name,
                          cost_per_day,
                          last_modified,
                          created_date,
                          mod_user,
                          dml_flag,
                          version)
                          values(
                          :old.category_id,
                          :old.category_name,
                          :old.cost_per_day,                      
                          sysdate,
                          :old.created_date,
                          sys_context('USERENV','OS_USER'),
                          'D',
                          :old.version + 1
                          );
        else
          insert into car_categories_h(category_id,
                             category_name,
                             cost_per_day,
                             last_modified,
                             created_date,
                             mod_user,
                             dml_flag,
                             version)
                             values(
                             :new.category_id,
                             :new.category_name,
                             :new.cost_per_day,
                             :new.last_modified,
                             :new.created_date,
                             :new.mod_user,
                             :new.dml_flag,
                             :new.version
                             );
      end if;
end;