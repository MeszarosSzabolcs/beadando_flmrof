create or replace trigger clients_trg
before insert or update on clients
for each row 
begin 
    if(inserting) then
       if(:new.client_id is null) then
          :new.client_id := client_id_seq.nextval;
       end if;
       :new.created_date := sysdate;
       :new.last_modified := sysdate;
       :new.dml_flag := 'I';
       :new.version := 1;
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
    else
       :new.last_modified := sysdate;
       :new.dml_flag := 'U';
       :new.mod_user := sys_context(namespace => 'USERENV', attribute => 'OS_USER');
       :new.version := :old.version + 1;
    end if;
end;
/

create or replace trigger clients_h_trg
after delete or update or insert on clients
for each row
begin
    if (deleting) then
       insert into clients_h(client_id,
                              first_name,
                              last_name,
                              email,
                              tel,
                              birth_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.client_id, 
                             :old.first_name, 
                             :old.last_name, 
                             :old.email, 
                             :old.tel, 
                             :old.birth_date,
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into clients_h(client_id,
                              first_name,
                              last_name,
                              email,
                              tel,
                              birth_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.client_id,
                              :new.first_name,
                              :new.last_name,
                              :new.email,
                              :new.tel,
                              :new.birth_date,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;