CREATE TRIGGER car_id_seq_trigger
BEFORE INSERT ON rentable_cars
FOR EACH ROW
BEGIN
    IF(:new.car_id is null) then
    :new.car_id := CAR_ID_SEQ.nextval;
    END IF;
END;