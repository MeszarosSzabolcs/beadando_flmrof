CREATE TRIGGER location_id_seq_trigger
BEFORE INSERT ON car_locations
FOR EACH ROW
BEGIN
    IF(:new.location_id is null) then
    :new.location_id := LOCATION_ID_SEQ.nextval;
    END IF;
END;