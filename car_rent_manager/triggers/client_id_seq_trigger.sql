CREATE TRIGGER client_id_seq_trigger
BEFORE INSERT ON clients
FOR EACH ROW
BEGIN
    IF(:new.client_id is null) then
    :new.client_id := CLIENT_ID_SEQ.nextval;
    END IF;
END;
