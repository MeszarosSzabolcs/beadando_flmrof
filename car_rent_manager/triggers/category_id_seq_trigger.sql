CREATE TRIGGER category_id_seq_trigger
BEFORE INSERT ON car_categories
FOR EACH ROW
BEGIN
    IF(:new.category_id is null) then
    :new.category_id := CATEGORY_ID_SEQ.nextval;
    END IF;
END;