create or replace package get_data_pkg is

  
  function get_cars
    return ty_car_array;

  function get_cars_per_location(p_location_id in number)
    return ty_cpl_array;

  function get_cars_per_category(p_category_id in number)
    return ty_cpc_array;

  function get_clients 
  return ty_client_array;
  
end;
/
create or replace package body get_data_pkg is

  function get_cars_per_location(p_location_id number)
    return ty_cpl_array is
    cpl_array ty_cpl_array;
  begin
    cpl_array := ty_cpl_array();
    for i in (select rc.car_id,
                     rc.make,
                     rc.model_name
                from rentable_cars rc join car_locations cl on (rc.location_id = cl.location_id)
               where rc.location_id = p_location_id) loop
      cpl_array.extend();
      cpl_array(cpl_array.count) := ty_cpl(p_car_id => i.car_id,
                                           p_make => i.make,
                                           p_model_name => i.model_name);
    end loop;
    return cpl_array;
  end;

  function get_cars_per_category(p_category_id number)
    return ty_cpc_array is
    cpc_array ty_cpc_array;
  begin
    cpc_array := ty_cpc_array();
    for i in (select cc.category_name,
                     rc.car_id,
                     rc.make,
                     rc.model_name
                from rentable_cars rc join car_categories cc on (rc.category_id = cc.category_id)
               where rc.category_id = p_category_id) loop
      cpc_array.extend();
      cpc_array(cpc_array.count) := ty_cpc(p_category_name => i.category_name,
                                           p_car_id => i.car_id,
                                           p_make => i.make,
                                           p_model_name => i.model_name);
    end loop;
    return cpc_array;
  end;

  function get_clients return ty_client_array is
           client_array ty_client_array;
    begin
      client_array := ty_client_array();
      for i in (select first_name, last_name, email, tel, birth_date
                from clients)
        loop
          client_array.extend();
          client_array(client_array.count) := ty_client(
                                        p_first_name => i.first_name,
                                        p_last_name => i.last_name,
                                        p_email => i.email,
                                        p_tel => i.tel,
                                        p_birth_date => i.birth_date);
        end loop;
    return client_array;
    end;

  function get_cars return ty_car_array is
           car_array ty_car_array;
    begin
      car_array := ty_car_array();
      for i in (select make, model_name, fuel, gears, mileage, seats, year
                from rentable_cars)
        loop
          car_array.extend();
          car_array(car_array.count) := ty_car(
                                        p_make => i.make,
                                        p_model_name => i.model_name,
                                        p_fuel => i.fuel,
                                        p_gears => i.gears,
                                        p_mileage => i.mileage,
                                        p_seats => i.seats,
                                        p_year => i.year);
        end loop;
    return car_array;
    end;
end;