declare
    cpc_array ty_cpc_array;
begin
    cpc_array := get_data_pkg.get_cars_per_category(p_category_id => 20);
    for i in 1 .. cpc_array.count
        loop
        dbms_output.put_line('Category: ' || cpc_array(i).p_category_name || ', ' || 
                             'Car ID: ' || cpc_array(i).p_car_id || ', ' ||
                             'Car: ' || cpc_array(i).p_make || ' ' ||
                             cpc_array(i).p_model_name);
        end loop;
    if cpc_array.count <= 0 then dbms_output.put_line('There aren''t any cars in the given category or the category doesn''t exists!');
    end if;
end;