declare
    client_array ty_client_array;
begin
    client_array := get_data_pkg.get_clients;
    for i in 1 .. client_array.count
        loop
        dbms_output.put_line(client_array(i).p_first_name || ' ' ||
                             client_array(i).p_last_name || ', ' ||
                             'E-mail address: ' || client_array(i).p_email || ', ' ||
                             'Telephone number: ' || client_array(i).p_tel || ', ' ||
                             'Birth date: ' || client_array(i).p_birth_date);
        end loop;
end;