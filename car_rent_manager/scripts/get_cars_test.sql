declare
    car_array ty_car_array;
begin
    car_array := get_data_pkg.get_cars;
    for i in 1 .. car_array.count
        loop
        dbms_output.put_line(car_array(i).p_make || ' ' || 
                             car_array(i).p_model_name || ', ' ||
                             'Fuel: ' || car_array(i).p_fuel || ', ' ||
                             'Gear: ' || car_array(i).p_gears || ', ' ||
                             'Mileage(km): ' || car_array(i).p_mileage || ', ' ||
                             'Seats: ' || car_array(i).p_seats || ', ' ||
                             'Year: ' || car_array(i).p_year);
        end loop;
end;
