declare
    cpl_array ty_cpl_array;
begin
    cpl_array := get_data_pkg.get_cars_per_location(p_location_id => 10100);
    for i in 1 .. cpl_array.count
        loop
        dbms_output.put_line('Car ID: ' || cpl_array(i).p_car_id || ', ' ||
                             'Car: ' || cpl_array(i).p_make || ' ' ||
                             cpl_array(i).p_model_name);
        end loop;
    if cpl_array.count <= 0 then dbms_output.put_line('There aren''t any cars in the given location or the location doesn''t exists!');
    end if;
end;