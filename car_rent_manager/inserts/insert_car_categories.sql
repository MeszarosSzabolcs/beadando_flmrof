INSERT INTO car_categories(category_name, cost_per_day)
VALUES('Sedan', 5000);

INSERT INTO car_categories(category_name, cost_per_day)
VALUES('SUV', 6500);

INSERT INTO car_categories(category_name, cost_per_day)
VALUES('Van', 10000);

INSERT INTO car_categories(category_name, cost_per_day)
VALUES('Hatchback', 4500);

INSERT INTO car_categories(category_name, cost_per_day)
VALUES('Coupe', 7000);

INSERT INTO car_categories(category_name, cost_per_day)
VALUES('Sports Car', 15000);

