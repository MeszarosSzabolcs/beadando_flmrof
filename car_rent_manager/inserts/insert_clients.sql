INSERT INTO clients(first_name, last_name, email, tel, birth_date)
VALUES('Winston', 'Smith', 'WinstonSmith@gmail.com', '06-01-020-3040', to_date('1984-04-01', 'yyyy-mm-dd'));

INSERT INTO clients(first_name, last_name, email, tel, birth_date)
VALUES('Jack', 'Daniels', 'JackDaniels@gmail.com', '06-02-030-4050', to_date('1966-08-07', 'yyyy-mm-dd'));

INSERT INTO clients(first_name, last_name, email, tel, birth_date)
VALUES('Jennifer', 'Williams', 'JenniferWilliams@gmail.com', '06-03-040-5060', to_date('2000-10-10', 'yyyy-mm-dd'));

INSERT INTO clients(first_name, last_name, email, tel, birth_date)
VALUES('Karen', 'Davies', 'KarenDavies@gmail.com', '06-04-050-6070', to_date('1989-12-30', 'yyyy-mm-dd'));

INSERT INTO clients(first_name, last_name, email, tel, birth_date)
VALUES('Thomas', 'Wilson', 'ThomasWilsom@gmail.com', '06-05-060-7080', to_date('1955-05-22', 'yyyy-mm-dd'));

INSERT INTO clients(first_name, last_name, email, tel, birth_date)
VALUES('Amy', 'Brown', 'AmyBrown@gmail.com', '06-06-070-8090', to_date('1977-07-13', 'yyyy-mm-dd'));

INSERT INTO clients(first_name, last_name, email, tel, birth_date)
VALUES('Donald', 'Taylor', 'DonaldTaylor@gmail.com', '06-07-080-9010', to_date('1966-05-18', 'yyyy-mm-dd'));