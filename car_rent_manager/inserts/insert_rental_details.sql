INSERT INTO rental_details(client_id, car_id, from_date, ret_date)
VALUES(2, 108, to_date('2021-08-08', 'yyyy-mm-dd'), to_date('2021-08-17', 'yyyy-mm-dd'));

INSERT INTO rental_details(client_id, car_id, from_date, ret_date)
VALUES(4, 110, to_date('2021-09-10', 'yyyy-mm-dd'), to_date('2021-09-20', 'yyyy-mm-dd'));

INSERT INTO rental_details(client_id, car_id, from_date, ret_date)
VALUES(2, 101, to_date('2021-11-01', 'yyyy-mm-dd'), to_date('2021-12-01', 'yyyy-mm-dd'));

INSERT INTO rental_details(client_id, car_id, from_date, ret_date)
VALUES(6, 107, to_date('2022-01-02', 'yyyy-mm-dd'), to_date('2022-01-07', 'yyyy-mm-dd'));

INSERT INTO rental_details(client_id, car_id, from_date, ret_date)
VALUES(7, 109, to_date('2021-04-07', 'yyyy-mm-dd'), to_date('2021-04-20', 'yyyy-mm-dd'));

INSERT INTO rental_details(client_id, car_id, from_date, ret_date)
VALUES(1, 104, to_date('2021-12-12', 'yyyy-mm-dd'), to_date('2021-12-13', 'yyyy-mm-dd'));