INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(10, '318i', 'BMW', 'petrol', 'manual', 230000, 5, 1999, 10100);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(10, '407 2.0 Symbole', 'Peugeot', 'petrol', 'automatic', 142000, 5, 2004, 10200);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(20, 'Vitara 1.4 GL+ Hybrid', 'Suzuki', 'petrol', 'manual', 20000, 5, 2021, 10300);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(20, 'Kuga 1.5 EcoBoost Titanium Technology', 'Ford', 'petrol', 'manual', 80000, 5, 2018, 10400);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(30, 'Transit 2.0 TDI 300 S TDE', 'Ford', 'diesel', 'manual', 295000, 9, 2003, 10100);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(30, 'Jumper 2.0 HDI 27 CH', 'Citroen', 'diesel', 'manual', 300000, 9, 2003, 10200);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(40, 'Golf VII 2.0 TDI Highline', 'Volkswagen', 'diesel', 'manual', 151000, 5, 2013, 10100);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(40, 'Astra K 1.2 T Busieness Edition', 'Opel', 'petrol', 'manual', 22500, 5, 2020, 10300);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(50, 'Supra GR 3.0 - Premium Paket', 'Toyota', 'petrol', 'manual', 34500, 2, 2019, 10400);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(50, 'Cordoba 1.6 Sportline IV', 'Seat', 'petrol', 'manual', 241000, 5, 2001, 10400);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(60, 'GT-R Black Edition', 'Nissan', 'petrol', 'automatic', 32895, 4, 2010, 10500);

INSERT INTO rentable_cars(category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
VALUES(60, 'Firebird 3.4 V6', 'Pontiac', 'petrol', 'manual', 191000, 4, 1994, 10500);
