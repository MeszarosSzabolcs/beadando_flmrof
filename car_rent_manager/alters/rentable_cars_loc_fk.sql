ALTER TABLE rentable_cars
ADD CONSTRAINT rentable_cars_loc_fk FOREIGN KEY (location_id) REFERENCES car_locations(location_id);