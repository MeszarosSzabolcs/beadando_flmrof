ALTER TABLE rental_details
ADD CONSTRAINT rental_details_car_fk FOREIGN KEY (car_id) REFERENCES rentable_cars(car_id);