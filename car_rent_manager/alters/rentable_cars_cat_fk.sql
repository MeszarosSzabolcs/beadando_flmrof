ALTER TABLE rentable_cars
ADD CONSTRAINT rentable_cars_cat_fk FOREIGN KEY (category_id) REFERENCES car_categories(category_id);
