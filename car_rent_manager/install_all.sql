SET SERVEROUTPUT ON;

SPOOL .\install.log


PROMPT Installing DB...

-- Install sequences
PROMPT installing SEQUENCES...
@./sequences/car_id_seq.sql
@./sequences/category_id_seq.sql
@./sequences/client_id_seq.sql
@./sequences/location_id_seq.sql
@./sequences/rent_id_seq.sql

-- Install tables
PROMPT installing TABLES...
@./tables/car_categories_table.sql
@./tables/car_locations_table.sql
@./tables/clients_table.sql
@./tables/rentable_cars_table.sql
@./tables/rental_details_table.sql

@./tables/car_categories_table_h.sql
@./tables/car_locations_table_h.sql
@./tables/clients_table_h.sql
@./tables/rentable_cars_table_h.sql
@./tables/rental_details_table_h.sql

-- Install types
PROMPT installig TYPES...
@./types/ty_car_typ.sql
@./types/ty_client_typ.sql
@./types/ty_cpc_typ.sql
@./types/ty_cpl_typ.sql

@./types/ty_car_array_typ.sql
@./types/ty_client_array_typ.sql
@./types/ty_cpc_array_typ.sql
@./types/ty_cpl_array_typ.sql

-- Packages
PROMPT installing PACKAGES...
@./packages/get_data_pkg.sql

-- Procedures
PROMPT installing PROCEDURES...
@./procedures/add_car_category.sql
@./procedures/add_car_location.sql
@./procedures/add_client.sql
@./procedures/add_rentable_car.sql
@./procedures/add_rental_detail.sql

-- Views
PROMPT installing VIEWS...
@./views/vw_car_details.sql
@./views/vw_car_location_details.sql
@./views/vw_rental_informations.sql

-- Triggers
PROMPT installing TRIGGERS...
@./triggers/car_id_seq_trigger.sql
@./triggers/category_id_seq_trigger.sql
@./triggers/location_id_seq_trigger.sql
@./triggers/rent_id_seq_trigger.sql
@./triggers/client_id_seq_trigger.sql

@./triggers/car_categories_trg.sql
@./triggers/car_locations_trg.sql
@./triggers/clients_trg.sql
@./triggers/rental_details_trg.sql
@./triggers/rentable_cars_trg.sql

-- Alters
PROMPT installing ALTERS...
-- PKs
@./alters/car_categories_pk.sql
@./alters/car_location_pk.sql
@./alters/clients_pk.sql
@./alters/rentable_cars_pk.sql
@./alters/rental_details_pk.sql
-- FKs
@./alters/rentable_cars_cat_fk.sql
@./alters/rentable_cars_loc_fk.sql
@./alters/rental_details_car_fk.sql
@./alters/rental_details_cli_fk.sql

COMMIT;

-- Recompile schema
BEGIN
  dbms_utility.compile_schema(schema => 'CAR_RENT_MANAGER');
END;
/

-- Table data
PROMPT installing TABLE DATA...
@./inserts/insert_car_categories.sql
@./inserts/insert_car_locations.sql
@./inserts/insert_clients.sql
@./inserts/insert_rentable_cars.sql
@./inserts/insert_rental_details.sql

PROMPT Done.

SPOOL OFF;
