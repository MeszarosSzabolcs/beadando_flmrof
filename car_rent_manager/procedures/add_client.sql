create or replace procedure add_client(p_first_name varchar2,
                                       p_last_name varchar2,
                                       p_email varchar2,
                                       p_tel varchar2,
                                       p_birth_date varchar2) is
begin
  insert into clients
    (first_name, last_name, email, tel, birth_date)
  values
    (p_first_name,
     p_last_name,
     p_email,
     p_tel,
     to_date(p_birth_date, 'YYYY-MM-DD'));
end;