create or replace procedure add_rentable_car(p_category_id number,
                                             p_model_name varchar2,
                                             p_make varchar2,
                                             p_fuel varchar2,
                                             p_gears varchar2,
                                             p_mileage number,
                                             p_seats number,
                                             p_year number,
                                             p_location_id number) is
p_category_not_found number;
p_location_not_found number;
category_not_found exception;
location_not_found exception;
pragma exception_init(category_not_found, -20001);
pragma exception_init(location_not_found, -20002);

begin
    select count(*)
    into p_category_not_found
    from car_categories
    where category_id = p_category_id;

    select count(*)
    into p_location_not_found
    from car_locations
    where location_id = p_location_id;

    if p_category_not_found < 1 then
    raise_application_error(-20001, 'The given category id is not found.');
    
    elsif p_location_not_found < 1 then
    raise_application_error(-20002, 'The given location id is not found.');

    else

  insert into rentable_cars
    (category_id, model_name, make, fuel, gears, mileage, seats, year, location_id)
  values
    (p_category_id,
     p_model_name,
     p_make,
     p_fuel,
     p_gears,
     p_mileage,
     p_seats,
     p_year,
     p_location_id);
     
     end if;
end;