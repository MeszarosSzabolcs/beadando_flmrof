create or replace procedure add_car_category(p_category_name varchar2,
                                             p_cost_per_day number) is
begin
  insert into car_categories
    (category_name, cost_per_day)
  values
    (p_category_name,
     p_cost_per_day);
end;