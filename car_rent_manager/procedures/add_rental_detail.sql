create or replace procedure add_rental_detail(p_client_id number,
                                              p_car_id varchar2,
                                              p_from_date varchar2,
                                              p_ret_date varchar2) is
p_client_not_found number;
p_car_not_found number;
client_not_found exception;
car_not_found exception;
pragma exception_init(client_not_found, -20003);
pragma exception_init(car_not_found, -20004);

begin
    select count(*)
    into p_car_not_found
    from rentable_cars
    where car_id = p_car_id;
    
    select count(*)
    into p_client_not_found
    from clients
    where client_id = p_client_id;

    if p_car_not_found < 1 then
    raise_application_error(-20004, 'The given car id is not found.');

    elsif p_client_not_found < 1 then
    raise_application_error(-20003, 'The given client id is not found.');

    else
    
  insert into rental_details
    (client_id, car_id, from_date, ret_date)
  values
    (p_client_id,
     p_car_id,
     to_date(p_from_date, 'YYYY-MM-DD'),
     to_date(p_ret_date, 'YYYY-MM-DD'));
     
     end if;
end;