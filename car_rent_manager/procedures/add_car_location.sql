create or replace procedure add_car_location(p_city varchar2,
                                             p_street_name varchar2,
                                             p_building_number number,
                                             p_zip_code number) is
begin
  insert into car_locations
    (city, street_name, building_number, zip_code)
  values
    (p_city,
     p_street_name,
     p_building_number,
     p_zip_code);
end;