CREATE VIEW vw_car_details AS
SELECT rc.car_id, rc.make, rc.model_name, cc. category_id, cc.category_name, rc.fuel, rc.gears, rc.mileage, rc.year, cc.cost_per_day
FROM car_categories cc JOIN rentable_cars rc on (cc.category_id = rc.category_id)
ORDER BY rc.car_id;
