CREATE VIEW vw_rental_informations AS
SELECT DISTINCT c.last_name || ' ' || c.first_name AS "Full name", rc.car_id, rc.make, rc.model_name, cc.category_id, cc.category_name, cc.cost_per_day, rd.from_date, rd.ret_date, rd.ret_date - rd.from_date as "Total days", (rd.ret_date - rd.from_date) * cc.cost_per_day as "Total cost"  
FROM car_locations cl JOIN rentable_cars rc on (cl.location_id = rc.location_id) JOIN car_categories cc on (cc.category_id = rc.category_id) JOIN rental_details rd on (rd.car_id = rc.car_id) JOIN clients c on (c.client_id = rd.client_id)
ORDER BY "Full name";