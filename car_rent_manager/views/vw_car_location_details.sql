CREATE VIEW vw_car_location_details AS
SELECT rc.car_id, rc.make, rc.model_name, cl.location_id, cl.zip_code || ', ' || cl.city || ' ' || cl.street_name || ' ' ||  cl.building_number || '.' AS "Location"
FROM car_locations cl JOIN rentable_cars rc on (cl.location_id = rc.location_id)
ORDER BY rc.car_id;