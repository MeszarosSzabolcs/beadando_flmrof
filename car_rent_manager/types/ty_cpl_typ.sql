create or replace type ty_cpl as object
(
  p_car_id number,
  p_make varchar2(30),
  p_model_name varchar2(50)
)