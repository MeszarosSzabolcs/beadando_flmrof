create or replace type ty_client as object
(
  p_first_name varchar2(50),
  p_last_name varchar2(50),
  p_email varchar2(80),
  p_tel varchar2(20),
  p_birth_date date
)