create or replace type ty_cpc as object
(
  p_category_name varchar2(30),
  p_car_id number,
  p_make varchar2(30),
  p_model_name varchar2(50)
)