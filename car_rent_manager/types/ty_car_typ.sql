create or replace type ty_car as object
(
  p_make varchar2(30),
  p_model_name varchar2(50),
  p_fuel varchar2(15),
  p_gears varchar2(20),
  p_mileage number,
  p_seats number,
  p_year number
)