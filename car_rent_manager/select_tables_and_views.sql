-- TABLES
select * from car_categories;
select * from car_categories_h;
select * from car_locations;
select * from car_locations_h;
select * from clients;
select * from clients_h;
select * from rentable_cars;
select * from rentable_cars_h;
select * from rental_details;
select * from rental_details_h;

-- VIEWS
select * from vw_car_details;
select * from vw_car_location_details;
select * from vw_rental_informations;
