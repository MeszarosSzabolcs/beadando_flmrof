create table clients_h (
    client_id number,
    first_name varchar2(50),
    last_name varchar2(50),
    email varchar2(80),
    tel varchar2 (20),
    birth_date date,
    last_modified date,
    created_date date,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);