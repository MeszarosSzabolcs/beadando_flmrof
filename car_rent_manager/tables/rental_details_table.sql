create table rental_details (
    rent_id number not null,
    client_id number not null,
    car_id number not null,
    from_date date not null,
    ret_date date not null,
    last_modified date default sysdate not null,
    created_date date default sysdate not null,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);