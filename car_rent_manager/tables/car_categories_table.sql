create table car_categories (
    category_id number not null,
    category_name varchar2(30) not null,
    cost_per_day number not null,
    last_modified date default sysdate not null,
    created_date date default sysdate not null,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);