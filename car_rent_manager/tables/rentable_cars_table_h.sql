create table rentable_cars_h (
    car_id number,
    category_id number,
    model_name varchar2(50),
    make varchar2(30),
    fuel varchar2(15),
    gears varchar2(10),
    mileage number,
    seats number,
    year number,
    location_id number,
    last_modified date,
    created_date date,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);