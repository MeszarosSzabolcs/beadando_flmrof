create table rental_details_h (
    rent_id number,
    client_id number,
    car_id number,
    from_date date,
    ret_date date,
    last_modified date,
    created_date date,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);