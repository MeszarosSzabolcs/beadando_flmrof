create table rentable_cars (
    car_id number not null,
    category_id number not null,
    model_name varchar2(50) not null,
    make varchar2(30) not null,
    fuel varchar2(15) not null,
    gears varchar2(10) not null,
    mileage number not null,
    seats number not null,
    year number not null,
    location_id number not null,
    last_modified date default sysdate not null,
    created_date date default sysdate not null,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);