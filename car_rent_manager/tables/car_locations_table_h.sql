create table car_locations_h (
    location_id number,
    city varchar2(50),
    street_name varchar2(100),
    building_number number,
    zip_code number,
    last_modified date,
    created_date date,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);