alter session set "_ORACLE_SCRIPT"=true; 

DECLARE 
    CURSOR cur IS 
      SELECT 'alter system kill session ''' 
             || sid 
             || ',' 
             || serial# 
             || '''' AS command 
      FROM   v$session 
      WHERE  username = 'CAR_RENT_MANAGER'; 
BEGIN 
    FOR c IN cur LOOP 
        EXECUTE IMMEDIATE c.command; 
    END LOOP; 
END; 

/ 

DECLARE
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count FROM dba_users t WHERE t.username='CAR_RENT_MANAGER';
  IF v_count = 1 THEN 
    EXECUTE IMMEDIATE 'DROP USER car_rent_manager CASCADE';
  END IF;
END;

/

CREATE USER car_rent_manager 
  IDENTIFIED BY "12345678" 
  DEFAULT TABLESPACE users
  QUOTA UNLIMITED ON users
;

GRANT CREATE SESSION TO car_rent_manager;
GRANT CREATE TABLE TO car_rent_manager;
GRANT CREATE VIEW TO car_rent_manager;
GRANT CREATE TRIGGER TO car_rent_manager;
GRANT CREATE SEQUENCE TO car_rent_manager;
GRANT CREATE PROCEDURE TO car_rent_manager;
GRANT CREATE TYPE TO car_rent_manager;

ALTER SESSION SET CURRENT_SCHEMA=car_rent_manager;
